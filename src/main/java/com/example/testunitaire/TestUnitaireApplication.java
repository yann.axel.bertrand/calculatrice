package com.example.testunitaire;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestUnitaireApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestUnitaireApplication.class, args);
        Calculatrice calculatrice = new Calculatrice();
        calculatrice.additionner(5,5);
    }

}
