package com.example.testunitaire;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculatriceTest {
    private Calculatrice calculatrice;

    @BeforeEach
    void setUp(){
        this.calculatrice = new Calculatrice();
    }

    @Test
    @DisplayName("addition doit fonctionner")
    void testAdditionner() {
        assertEquals(15, calculatrice.additionner(10,5), "addition de 10 et 5 doit donner 15");
    }

    @Test
    @DisplayName("multiplication doit fonctionner")
    void testMultiplier() {
        assertEquals(45, calculatrice.multiplier(3,15), "3*15=15");
    }

    @Test
    @DisplayName("multiplication par 0 doit retourner 0")
    void testMultiplierParZero() {
        assertEquals(0, calculatrice.multiplier(42,0), "42*0=0");
    }

//    @Test
//    @DisplayName("diviser par 0 doit echouer")
//    void testDivisierParZero() {
//        assertEquals(ArithmeticException.class() -> calculatrice.diviser(42,0), "division/0=erreur");
//    }
}